# APPLICATION

## DEVELOPER INFO

name: Yulia Aleksandrova

email: yaleksandrova@yandex.ru

email: yaleksandrova@tsconsulting.com

## HARDWARE

CPU: i5

RAM: 16Gb


## SOFTWARE

OS: Windows 10

JDK: 15.0.1

## APPLICATION BUILD

```
mvn clean install
```

## APPLICATION RUN

```
java -jar ./task-manager.jar
```

## SCREENSHOTS

https://disk.yandex.ru/d/humZ7Ts4d2DPbg?w=1
