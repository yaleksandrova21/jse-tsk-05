package ru.yaleksandrova.tm.constant;

public class ApplicationConst {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";
}
